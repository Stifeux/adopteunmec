<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Badge;
use AppBundle\Entity\Profil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


/**
 * @Route("/api/v1")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/badges", name="get_badges")
     */
    public function getBadgesAction(Request $request)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $badges = json_decode($serializer->serialize($this->getDoctrine()->getRepository(Badge::class)->findAll(),'json'));
        
        $response = new JsonResponse([
            'status' => 200,
            'response' => $badges
        ]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/badges/{idBadge}", name="get_badge")
     */
    public function getBadgeAction(Request $request, $idBadge)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $badges = json_decode($serializer->serialize($this->getDoctrine()->getRepository(Badge::class)->findOneById($idBadge),'json'));
        $response = new JsonResponse([
            'status' => 200,
            'response' => $badges
        ]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/profils", name="get_profiles")
     */
    public function getProfilesAction(Request $request)
    {

        $profils = $this->getDoctrine()->getRepository(Profil::class)->findAll();

        $profilsJson = array();

        foreach ($profils as $key => $profil) {
            $profilsJson[$key] = $this->serializeProfil($profil);
        }

        shuffle($profilsJson);

        $response = new JsonResponse([
            'status' => 200,
            'response' => $profilsJson
        ]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/profils/{idProfil}", name="get_profil")
     */
    public function getProfilAction(Request $request, $idProfil)
    {
        $profil = $this->getDoctrine()->getRepository(Profil::class)->findOneById($idProfil);

        $profilJson = $this->serializeProfil($profil);

        if(isset($profil)){
            $response =  new JsonResponse([
                'status' => 200,
                'response' => $profilJson
            ]);
        }
        else{
            $response =  new JsonResponse([
                'status' => 404,
                'response' => 'Profil not found.'
            ]);
        }

        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    public function serializeProfil($profil)
    {
        $profilJson = $profil->jsonSerialize();
        $profilJson['badges'] = null;
        foreach($profil->getProfilBadges() as $keyBadge => $profilBadge) {
            $profilJson['badges'][$keyBadge]['id'] = $profilBadge->getBadge()->getId();
            $profilJson['badges'][$keyBadge]['enable'] = $profilBadge->getEnable();
        }
        $profilJson['comments'] = array();
        foreach($profil->getComments() as $keyComment => $comment) {
            $profilJson['comments'][$keyComment] = $comment->jsonSerialize();
        }
        $profilJson['projects'] = array();
        foreach($profil->getProjects() as $keyProject => $project) {
            $profilJson['projects'][$keyProject] = $project->jsonSerialize();
        }

        return $profilJson;
    }

    /**
     * @Route("/profils/{idProfil}/images/picture")
     */
    public function imageAction(Request $request, $idProfil) {
        $path = $this->get('kernel')->getRootDir().'/../web/assets/profils/pictures/'.$idProfil.'.png';
        return new BinaryFileResponse($path);
    }

    /**
     * @Route("/badges/{idBadge}/image")
     */
    public function badgeImageAction(Request $request, $idBadge) {
        $badge = $this->getDoctrine()->getRepository(Badge::class)->findOneById($idBadge);
        $idImage = $badge->getImg();
        $path = $this->get('kernel')->getRootDir().'/../web/assets/badges/'.$idImage;
        return new BinaryFileResponse($path);
    }
}

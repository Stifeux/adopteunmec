<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use AppBundle\Entity\Profil;
use AppBundle\Form\ProfilType;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/user/profil")
 */
class ProfilController extends Controller
{
    /**
     * @Route("/edit", name="profil.edit")
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $profil = $user->getProfil();
        if($profil == null)
            $profil = new Profil();

        $form = $this->createForm(ProfilType::class, $profil);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $profil->setAdopted(false);
            $profil->setUser($user);
            $cv = $form['pdfCv']->getData();
            dump($cv);
            if ($cv) {
                $originalFilename = pathinfo($cv->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$cv->guessExtension();

                // Move the file to the directory where cv are stored
                try {
                    $cv->move(
                        $this->getParameter('cv_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $profil->setPdfCv($newFilename);
            }

            $pictures = $form['img']->getData();
            if ($pictures) {
                $originalFilename = pathinfo($pictures->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$pictures->guessExtension();

                // Move the file to the directory where cv are stored
                try {
                    $pictures->move(
                        $this->getParameter('photo_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $profil->setImg($newFilename);
            }
            $em->persist($profil);
            // $em->flush();
            $user->setProfil($profil);
            $em->persist($user);
            $em->flush();
        }

        $form = $form->createView();

        return $this->render('@App/Profil/edit.html.twig', compact('form'));
    }

}

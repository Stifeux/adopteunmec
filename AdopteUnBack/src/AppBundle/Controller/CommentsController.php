<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Profil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/comments")
 */
class CommentsController extends Controller
{
    /**
     * @Route("/", name="comments")
     */
    public function commentsAction(Request $request)
    {
        $comments = $this->getDoctrine()->getRepository(Comment::class)->findAll();

        return $this->render('@App/Security/gestion_comments.html.twig', [
            'comments' => $comments,
        ]);
    }

//    TODO :
//        - bouger setCommentAction() vers DefaultController (API)
    /**
     * @Route("/setcomment", name="setComment")
     */
    public function setCommentAction(Request $request, EntityManagerInterface $em)
    {
        $frontComment = $request->request->all();
        $comment = new Comment();
        $date = new \DateTime('now');
        $profil = $this->getDoctrine()->getRepository(Profil::class)->findById($frontComment["id"]);
        $comment->setPostedBy($frontComment["auteur"]);
        $comment->setText($frontComment["texte"]);
        $comment->setCreatedAt($date);
        $comment->setValidated(false);
        $comment->setProfil($profil);

        $em->persist($comment);
        $em->flush();

        return new Response("Commentaire bien ajouté", Response::HTTP_OK);
    }

    /**
     * @Route("/getcomments/{id}", name="getCommentForProfil")
     */
    public function getCommentsForProfilAction(Request $request, EntityManagerInterface $em)
    {
        $id = $request->get("id");
        $profil = $this->getDoctrine()->getRepository(Profil::class)->findById($id);

        $comments = $this->getDoctrine()->getRepository(Comment::class)->findBy(array("profil",$profil));

        return $this->render('@App/Security/gestion_comments.html.twig', [
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/acceptcomment/{id}", name="acceptComment")
     */
    public function acceptCommentsAction(Request $request, EntityManagerInterface $em)
    {
        $id = $request->get("id");

        $comment = $this->getDoctrine()->getRepository(Comment::class)->findOneById($id);

        if (!$comment) {
            throw $this->createNotFoundException(
                'No comment found for id '.$id
            );
        }

        $comment->setValidated(true);
        $em->flush();
        if($request->isXmlHttpRequest()){
            return new JsonResponse([
                'status' => 200,
                'response' => 'Comment accepted.'
            ]);
        }
        return $this->redirectToRoute('comments');
    }

    /**
     * @Route("/denyComment/{id}", name="denyComment")
     */
    public function denyCommentsAction(Request $request, EntityManagerInterface $em)
    {
        $id = $request->get("id");

        $comment = $this->getDoctrine()->getRepository(Comment::class)->findOneById($id);

        if (!$comment) {
            throw $this->createNotFoundException(
                'No comment found for id '.$id
            );
        }

        $em->remove($comment);
        $em->flush();
        if($request->isXmlHttpRequest()){
            return new JsonResponse([
                'status' => 200,
                'response' => 'Comment deleted.'
            ]);
        }
        return $this->redirectToRoute('comments');
    }
}

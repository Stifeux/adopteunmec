CREATE TABLE `user` (
  `id` int,
  `username` varchar(255),
  `password` varchar(255),
  `mail` varchar(255),
  `created_at` timestamp
);

CREATE TABLE `groupe` (
  `id` int,
  `role` varchar(255)
);

CREATE TABLE `profil` (
  `id` int,
  `lastName` varchar(255),
  `firstName` varchar(255),
  `birthday` date,
  `phone` varchar(255),
  `fb` varchar(255),
  `lkdn` varchar(255),
  `twtr` varchar(255),
  `insta` varchar(255),
  `github` varchar(255),
  `gitlab` varchar(255),
  `so` varchar(255),
  `other` json,
  `pdfCv` varchar(255),
  `linkCv` varchar(255),
  `video` varchar(255),
  `adopted` boolean,
  `promo` varchar(255)
);

CREATE TABLE `comment` (
  `id` int,
  `from` varchar(255),
  `text` longtext,
  `validated` boolean,
  `created_at` timestamp
);

CREATE TABLE `project` (
  `id` int,
  `snapshot` varchar(255),
  `link` varchar(255),
  `name` varchar(255),
  `desc` varchar(255)
);

CREATE TABLE `badge` (
  `id` int,
  `name` varchar(255),
  `img` varchar(255)
);

ALTER TABLE `profil` ADD FOREIGN KEY (`id`) REFERENCES `user` (`id`);

ALTER TABLE `project` ADD FOREIGN KEY (`id`) REFERENCES `profil` (`id`);

ALTER TABLE `badge` ADD FOREIGN KEY (`id`) REFERENCES `profil` (`id`);

ALTER TABLE `comment` ADD FOREIGN KEY (`id`) REFERENCES `profil` (`id`);

ALTER TABLE `groupe` ADD FOREIGN KEY (`id`) REFERENCES `user` (`id`);
